﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetwork.Models
{
    public class Message
    {
        public int MessageID { get; set; }
        public string Content { get; set; }
        public DateTime? DataSent { get; set; }
        public User UserSender { get; set; }
        public User UserReceiver { get; set; }
    }
}
