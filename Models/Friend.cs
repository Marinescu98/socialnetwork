﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetwork.Models
{
    public class Friend
    {
        public int FriendID { get; set; }
        public User FriendResponse { get; set; }
        public User FriendSent { get; set; }
        public Boolean AreFriends { get; set; }
    }
}
