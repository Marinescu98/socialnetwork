﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetwork.Models
{
    public class User
    {
        public int UserID { get; set; }
        public string Fullname { get; set; }
        public string Email { get; set; }
        public string Country { get; set; }
        public string Password { get; set; }
        public DateTime? BirthDay { get; set; }
        public string WebSite { get; set; }
        public string About { get; set; }
        public string CompanyWorking { get; set; }
        public string Genre { get; set; }
        public Photo PhotoProfile { get; set; }
        public Role UserRole { get; set; }
    }
}
