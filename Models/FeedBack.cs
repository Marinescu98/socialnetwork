﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetwork.Models
{
    public class FeedBack
    {
        public int FeedBackID { get; set; }
        public string Content { get; set; }
        public string Subject { get; set; }
        public User UserSent { get; set; }

        public DateTime? DateReceive { get; set; }
    }
}
