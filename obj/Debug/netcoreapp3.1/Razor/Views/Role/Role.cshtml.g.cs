#pragma checksum "C:\Users\marin\OneDrive\Documente\BitBucket\PAW-proiect\SocialNetwork\Views\Role\Role.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "1c29881e38d56971e7fd901ec27a9b9399cbbdf0"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Role_Role), @"mvc.1.0.view", @"/Views/Role/Role.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\marin\OneDrive\Documente\BitBucket\PAW-proiect\SocialNetwork\Views\_ViewImports.cshtml"
using SocialNetwork;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\marin\OneDrive\Documente\BitBucket\PAW-proiect\SocialNetwork\Views\_ViewImports.cshtml"
using SocialNetwork.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"1c29881e38d56971e7fd901ec27a9b9399cbbdf0", @"/Views/Role/Role.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"f753f9608fdc9f7cfcaef4fd6b8f21361e1f0383", @"/Views/_ViewImports.cshtml")]
    public class Views_Role_Role : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 2 "C:\Users\marin\OneDrive\Documente\BitBucket\PAW-proiect\SocialNetwork\Views\Role\Role.cshtml"
  
    ViewData["Title"] = "Role";
    Layout = "~/Views/Shared/_LayoutTabels.cshtml";

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
<script type=""text/javascript"">
    $(document).ready(function () {
        populateTable();
    })

    function populateTable() {

        $.noConflict();
        $('#DataTabelRole').DataTable({
            ""processing"": true,
            ""serverSide"": false,
            ""filter"": true,
            ""orderMulti"": false,
            ""pageLength"": 10,
            ""destroy"": true,
            ""ajax"":
            {
                ""url"": ""/Role/ReadRoles"",
                ""type"": ""GET"",
                ""datatype"": ""json"",
                ""dataSrc"": ''
            },
            ""columnDefs"":
                [{
                    ""targets"": [0],
                    ""visible"": true,
                    ""searchable"": false,
                }],
            ""columns"": [
                { ""data"": ""userID"", ""title"": ""ID"", ""autoWidth"": true },
                { ""data"": ""email"", ""title"": ""USER_EMAIL"", ""autoWidth"": true },
                { ""data"": ""userRole.name"", ""title"": ""ROLE"", ""autoW");
            WriteLiteral(@"idth"": true },
            ],
        });
    }


    function InsertRole(email, role) {
        if (email == """" || role == """" || role == ""--- Select role ---"") {
            alert(""All fields are required!"");
            return 0;
        }

        $.ajax({
            type: ""POST"",
            url: ""/Role/InsertRole"",
            data: {
                'Email': email,
                'USerRole.Name': role
            },
            dataType: ""json"",
            success: function (result) {
                var serverResponse = result.split("","");
                var responseCode = serverResponse[0];
                var responseMessage = serverResponse[1];

                alert(responseMessage);
                window.location.reload();

            }
        });


    }
</script>

<div class=""container col-lg-3 offset-sm-5"">
    <div class=""form-group"">
        <div class=""input-group"">
            <input class=""form-control"" type=""text"" name=""Email"" id=""EmailInsert"" p");
            WriteLiteral("laceholder=\"Email...\" />\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"form-group\">\r\n        <div class=\"input-group\">\r\n            <select class=\"form-control\" id=\"RoleInsert\">\r\n                ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "1c29881e38d56971e7fd901ec27a9b9399cbbdf05836", async() => {
                WriteLiteral("--- Select role ---");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n                ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "1c29881e38d56971e7fd901ec27a9b9399cbbdf06823", async() => {
                WriteLiteral("ADMIN");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n                ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "1c29881e38d56971e7fd901ec27a9b9399cbbdf07796", async() => {
                WriteLiteral("USER");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"
            </select>
        </div>
    </div>

    <div class=""form-group"">
        <button class=""btn btn-success form-control"" type=""submit"" onclick=""InsertRole($('#EmailInsert').val(),$('#RoleInsert').val())"">Add</button>
    </div>

</div>

<div class=""container"" style=""margin-top:30px"">
    <table class=""table"" id=""DataTabelRole"">
        <tr>
            <th>Id</th>
            <th>UserEmail</th>
            <th>Role</th>
        </tr>
    </table>
</div>


");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
