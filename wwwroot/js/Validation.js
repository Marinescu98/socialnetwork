﻿function ValidationEmail(email) {
    var expr = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/;
    if (!expr.test(email)) {
        return 'Invalid email address!';
    }
    return 0;
}

function ValidationPassword(password) {
    if (password.length < 6) {
        return 'Password must be at least 6 characters long!';
    }
    return 0;
}

function ValidationPasswordMatch(password1, password2) {
    if (password1 != password2) {
        return 'Passwords are different!';
    }
    return 0;
}