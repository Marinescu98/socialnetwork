﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SocialNetwork.Migrations
{
    public partial class migr3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UsersRole");

            migrationBuilder.AddColumn<int>(
                name: "UserRoleRoleID",
                table: "Users",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Users_UserRoleRoleID",
                table: "Users",
                column: "UserRoleRoleID");

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Roles_UserRoleRoleID",
                table: "Users",
                column: "UserRoleRoleID",
                principalTable: "Roles",
                principalColumn: "RoleID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_Roles_UserRoleRoleID",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Users_UserRoleRoleID",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "UserRoleRoleID",
                table: "Users");

            migrationBuilder.CreateTable(
                name: "UsersRole",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    idUserUserID = table.Column<int>(type: "int", nullable: true),
                    roleNameRoleID = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UsersRole", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UsersRole_Users_idUserUserID",
                        column: x => x.idUserUserID,
                        principalTable: "Users",
                        principalColumn: "UserID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UsersRole_Roles_roleNameRoleID",
                        column: x => x.roleNameRoleID,
                        principalTable: "Roles",
                        principalColumn: "RoleID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UsersRole_idUserUserID",
                table: "UsersRole",
                column: "idUserUserID");

            migrationBuilder.CreateIndex(
                name: "IX_UsersRole_roleNameRoleID",
                table: "UsersRole",
                column: "roleNameRoleID");
        }
    }
}
