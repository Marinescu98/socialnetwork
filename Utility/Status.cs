﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetwork.Utility
{
    public class Status
    {
        public static string DATABASE_ERROR_CODE = "500";

        public static string INVALID_PASSWORD_MESSAGE = "The password is incorrect!";
        public static string INVALID_PASSWORD_CODE = "1";

        public static string EMAIL_VALIDATION_MESSAGE = "The email already exist!";
        public static string EMAIL_VALIDATION_CODE = "2";

        public static string EMAIL_INVALID_MESSAGE = "The email is invalid!";
        public static string EMAIL_INVALID_CODE = "3";

        public static string NOT_FOUND_MESSAGE = "Not found!";
        public static string NOT_FOUND_CODE = "404";

        public static string SUCCESS_MESSAGE = "SUCCESS!";
        public static string SUCCESS_CODE = "200";

        public static string NO_CONTENT_MESSAGE = "No content!";
        public static string NO_CONTENT_CODE = "204";

        public static string BAD_REQUEST_MESSAGE = "Bad request!";
        public static string BAD_REQUEST_CODE = "400";

        public static string NOT_RESPONSE_MESSAGE = "Not response!";
        public static string NOT_RESPONSE_CODE = "444";
    }
}
