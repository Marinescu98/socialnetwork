﻿using Microsoft.EntityFrameworkCore;
using SocialNetwork.Models;
using SocialNetwork.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetwork.Repository.RepositoryImplementation
{
    public class FriendRepository: IFriendRepository
    {
        protected readonly SocialNetworkDBContext _context;
        public FriendRepository(SocialNetworkDBContext context)
        {
            _context = context;
        }

        public List<Friend> GetFriends(User currentUser)
        {
            List<Friend> friends = null;
            try
            {
                friends = _context.Friends.Include(u=>u.FriendResponse)
                    .Include(u => u.FriendSent)
                    .Include(u => u.FriendResponse.UserRole)
                    .Include(u => u.FriendResponse.PhotoProfile)
                    .Include(u=>u.FriendSent.PhotoProfile)
                    .Include(u=>u.FriendSent.UserRole)
                    .Where(u => (u.FriendResponse.UserID==currentUser.UserID || u.FriendSent.UserID==currentUser.UserID) && u.AreFriends==true)
                    .ToList();
            }
            catch (Exception e)
            {
            }

            return friends;
        }

        public List<User> GetUsersNotFriends()
        {
            List<User> users = null;
            try
            {
                users = _context.Users.FromSqlRaw("select * from Users join Friends on Friends.FriendResponseUserID!=Users.UserID and Friends.FriendSentUserID!=Users.UserID")
                    .Include(u => u.UserRole)
                    .Include(u => u.Country)
                    .Include(u => u.PhotoProfile)
                    .ToList();

            }
            catch (Exception e)
            {

            }
            return users;
        }

        public List<User> GetUsersNotFriendsByName(string name)
        {
            List<User> users = null;
            try
            {
                users = _context.Users.FromSqlRaw("select * from Users join Friends on Friends.FriendResponseUserID!=Users.UserID and Friends.FriendSentUserID!=Users.UserID")
                    .Include(u => u.UserRole)
                    .Include(u => u.PhotoProfile)
                    .Where(u=>u.Fullname.Contains(name)==true)
                    .ToList();

            }
            catch (Exception e)
            {

            }
            return users;
        }
    }
}
