﻿using Microsoft.EntityFrameworkCore;
using SocialNetwork.Repository.IRepository;
using SocialNetwork.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SocialNetwork.Repository.RepositoryImplementation
{
    public class BaseRepository<T> : IRepositoryBase<T> where T : class
    {
        protected readonly SocialNetworkDBContext _context;
        public BaseRepository(SocialNetworkDBContext context)
        {
            this._context = context;
        }
        public virtual int Insert(T Entity, out string errorMessage)
        {
            errorMessage = string.Empty;

            try
            {
                _context.Set<T>().Local.Add(Entity);

                if (_context.SaveChanges() > 0)
                    return 1;

            }
            catch (Exception e)
            {
                errorMessage = e.Message;
            }

            return 0;
        }

        public virtual T GetItem(Expression<Func<T, bool>> expression, out string errorMessage)
        {
            errorMessage = string.Empty;

            try
            {
                  T  entity = _context.Set<T>().Where(expression).FirstOrDefault(); 

                if (entity != null)
                    return entity;

            }
            catch (Exception e)
            {
                errorMessage = e.Message;
            }

            return null;
        }

        public virtual bool Delete(Expression<Func<T, bool>> expression, out string errorMessage)
        {
            errorMessage = string.Empty;
            T entity = null;

            try
            {
                entity = _context.Set<T>().Where(expression).FirstOrDefault();

                _context.Set<T>().Remove(entity);

                if (_context.SaveChanges() > 0)
                    return true;
            }
            catch (Exception e)
            {
                errorMessage = e.Message;
            }

            return false;
        }
        public virtual bool Update(T Entity, out string errorMessage)
        {
            errorMessage = string.Empty;

            try
            {
                _context.Set<T>().Update(Entity);

                if (_context.SaveChanges() > 0)
                    return true;

            }
            catch (Exception e)
            {
                errorMessage = e.Message;
            }

            return false;
        }
    }
}
