﻿using SocialNetwork.Models;
using SocialNetwork.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetwork.Repository.RepositoryImplementation
{
    public class PhotoRepository:BaseRepository<Photo>, IPhotoRepository
    {
        public PhotoRepository(SocialNetworkDBContext context):base(context)
        {

        }

        public override int Insert(Photo photo, out string errorMessage)
        {
            errorMessage = string.Empty;

            try
            {
                _context.Set<Photo>().Local.Add(photo);

                if (_context.SaveChanges() <= 0)
                    return -1;

                return _context.Set<Photo>().FirstOrDefault(u => u.Title == photo.Title).PhotoID;

            }
            catch (Exception e)
            {
                errorMessage = e.Message;
            }

            return -1;
        }

       
        //public bool InsertImage(Photo photo, User user, out string errorMessage)
        //{
        //    errorMessage = string.Empty;

        //    try
        //    {
        //        _context.Set<Photo>().Add(photo);

        //        if (_context.SaveChanges() <= 0)
        //            return false;

        //        Photo photoRetreive = _context.Photos.FirstOrDefault(u => u.Title == photo.Title && u.ImageData == photo.ImageData);

        //        user.PhotoProfile = photoRetreive;

        //        _context.Users.Update(user);

        //        if (_context.SaveChanges() > 0)
        //            return true;

        //    }
        //    catch (Exception e)
        //    {
        //        errorMessage = e.Message;
        //    }

        //    return false;
        //}
    }
}
