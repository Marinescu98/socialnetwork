﻿
using Microsoft.EntityFrameworkCore;
using SocialNetwork.Models;
using SocialNetwork.Repository.IRepository;
using SocialNetwork.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetwork.Repository.RepositoryImplementation
{
    public class FeedbackRepository :BaseRepository<FeedBack>, IFeedbackRepository
    {
        public FeedbackRepository(SocialNetworkDBContext context):base(context)
        {

        }

        public List<FeedBack> GetFeedbacks()
        {
            List<FeedBack> feedBacks = null;
            try
            {
                feedBacks = _context.Set<FeedBack>().Include(u=>u.UserSent).ToList();
            }
            catch (Exception e)
            {
            }

            return feedBacks;
        }
    }
}
