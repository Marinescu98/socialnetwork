﻿
using SocialNetwork.Models;
using SocialNetwork.Repository.IRepository;
using SocialNetwork.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetwork.Repository.RepositoryImplementation
{
    public class NoteRepository:BaseRepository<Note>,INoteRepository
    {
        public NoteRepository(SocialNetworkDBContext context) :base(context)
        {

        }
        public List<Note> GetNotes(string email)
        {
            List<Note> notes = null;
            try
            {
                 notes = _context.Notes.Where(a => a.User.Email == email).ToList();
            }
            catch (Exception e) { 
            }

            return notes;
        }

      
    }
}
