﻿using SocialNetwork.Repository.RepositoryImplementation;
using SocialNetwork.Models;
using SocialNetwork.Repository.IRepository;
using SocialNetwork.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;

namespace SocialNetwork.Repository.RepositoryImplementation
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        public UserRepository(SocialNetworkDBContext context) : base(context)
        {

        }
        public List<User> GetUsers()
        {
            List<User> userRoles = null;
            try
            {
                userRoles = _context.Users.Include(u => u.UserRole).Include(u => u.PhotoProfile).ToList();
            }
            catch (Exception e)
            {
            }

            return userRoles;
        }

        public bool UpdatePhoto(Photo photo, User user, out string errorMessage)
        {
            errorMessage = string.Empty;

            try
            {
                user.PhotoProfile.ImageData = photo.ImageData;
                user.PhotoProfile.Title = photo.Title;
                user.PhotoProfile.ImageDataUrl = photo.ImageDataUrl;

                _context.Set<Photo>().Update(user.PhotoProfile);

                if (_context.SaveChanges() > 0)
                    return true;
            }
            catch (Exception e)
            {
                errorMessage = e.Message;
            }

            return false;
        }
        public override User GetItem(Expression<Func<User, bool>> expression, out string errorMessage)
        {
            errorMessage = string.Empty;

            try
            {
                User entity = _context.Users.Where(expression).Include(u => u.UserRole).Include(u => u.PhotoProfile).FirstOrDefault();

                if (entity != null)
                    return entity;

            }
            catch (Exception e)
            {
                errorMessage = e.Message;
            }

            return null;
        }
       
    }
}

