﻿using SocialNetwork.Models;
using SocialNetwork.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetwork.Repository.RepositoryImplementation
{
    public class RoleRepostory : IRoleRepository
    {
        private readonly SocialNetworkDBContext _context;
        public RoleRepostory(SocialNetworkDBContext context)
        {
            _context = context;
        }
        public Role GetRoleById(int id, out string errorMessage)
        {
            errorMessage = string.Empty;

            try
            {
                Role entity = _context.Set<Role>().Where(u=>u.RoleID==id).FirstOrDefault();

                if (entity != null)
                    return entity;

            }
            catch (Exception e)
            {
                errorMessage = e.Message;
            }

            return null;
        }
    }
}
