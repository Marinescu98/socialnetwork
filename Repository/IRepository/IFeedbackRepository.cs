﻿using SocialNetwork.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetwork.Repository.IRepository
{
    public interface IFeedbackRepository:IRepositoryBase<FeedBack>
    {
        public List<FeedBack> GetFeedbacks();
    }
}
