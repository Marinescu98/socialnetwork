﻿using SocialNetwork.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetwork.Repository.IRepository
{
    public interface IPhotoRepository:IRepositoryBase<Photo>
    {
        public new int Insert(Photo photo, out string errorMessage);
    }
}
