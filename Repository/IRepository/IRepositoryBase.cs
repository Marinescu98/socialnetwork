﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SocialNetwork.Repository.IRepository
{
    public interface IRepositoryBase<T>
    {
        public int Insert(T Entity, out string errorMessage);
        public T GetItem(Expression<Func<T, bool>> expression, out string errorMessage);
        public bool Delete( Expression<Func<T, bool>> expression, out string errorMessage);
        public bool Update(T Entity, out string errorMessage);

    }
}
