﻿using SocialNetwork.Models;
using SocialNetwork.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetwork.Repository.IRepository
{
    public interface IUserRepository:IRepositoryBase<User>
    {
        //TODO sterge poza pe care o are curent
        public bool UpdatePhoto(Photo photo, User user, out string errorMessage);
        public List<User> GetUsers();
    }
}
