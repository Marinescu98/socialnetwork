﻿using SocialNetwork.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetwork.Repository.IRepository
{
    public interface INoteRepository:IRepositoryBase<Note>
    {
        public List<Note> GetNotes(string email);
    }
}
