﻿using SocialNetwork.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetwork.Repository.IRepository
{
    public interface IRoleRepository
    {
        public Role GetRoleById(int id, out string errorMessage);
    }
}
