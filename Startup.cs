using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SocialNetwork.Hubs;
using SocialNetwork.Services.IServices;
using SocialNetwork.Services.ServicesImplementation;

namespace SocialNetwork
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.AddRazorPages();
            services.AddSignalR();

            services.AddScoped<IRoleService, RoleServiceImplementation>();
            services.AddScoped<IUsersService, UsersServiceImplementation>();
            services.AddScoped<IFeedbackService, FeedbackServiceImplementation>();
            services.AddScoped<INotesService, NotesServiceImplementation>();
            services.AddScoped<IPhotoService, PhotoServiceImplementation>();
            services.AddScoped<IFriendService, FriendServiceImplementation>();
           // services.AddScoped<IMessageService, MessageServiceImplementation>();

            services.AddDistributedMemoryCache();
            services.AddSession();

            services.AddDbContext<SocialNetworkDBContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("SocialNetworkDB")));

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();
            app.UseSession();

            app.UseEndpoints(endpoints =>
            {
                //endpoints.MapControllerRoute(
                //    name: "default",
                //    pattern: "{controller=Home}/{action=Index}/{id?}");
                //endpoints.MapControllerRoute(
                //    name: "note",
                //    pattern: "{controller=Note}/{action=Index}/{id?}");
                endpoints.MapControllers();
                endpoints.MapRazorPages();
                endpoints.MapHub<ChatHub>("/chatHub");
            });
        }
    }
}
