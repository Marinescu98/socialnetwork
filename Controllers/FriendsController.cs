﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SocialNetwork.Models;
using SocialNetwork.Repository.IRepository;
using SocialNetwork.Services.IServices;

namespace SocialNetwork.Controllers
{
    public class FriendsController : Controller
    {
        private readonly IUsersService _usersService;
        private readonly IFriendService _friendsService;
        public FriendsController(IUsersService usersService, IFriendService friendsService)
        {
            _usersService = usersService;
            _friendsService = friendsService;
        }

        [Route("Friends")]
        [Route("Friends/Friends")]
        //TODO include toti utilizatorii, chiar si cei pe care ii are la prieteni
        public IActionResult Friends(string nameUser)
        {
            if (HttpContext.Session.GetString("Role") != null)
            {
                ViewBag.Role = HttpContext.Session.GetString("Role");
            }
            if (HttpContext.Session.GetString("Email") != "" && HttpContext.Session.GetString("Email") != null)
            {
                List<User> users = _friendsService.GetUsersNotFriendsByName(nameUser);
                string errorMessage = string.Empty;
                User user = _usersService.GetUserByEmail(HttpContext.Session.GetString("Email"), out errorMessage);
                ViewBag.Name = user.Fullname;
                return View(users);
            }
            return RedirectToAction("Home","Login");
        }
        //TODO Become friends + fereastra pentru notificari sa poata accepta cererea
        //TODO Lista de prieteni pentru a-i putea sterge
        [Route("Friends/BecomeFriends")]
        public IActionResult BecomeFriends()
        {
            string error = string.Empty;


            return View(error);
        }

    }
}