﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SocialNetwork.Models;
using SocialNetwork.Services.IServices;

namespace SocialNetwork.Controllers
{
    public class ChatController : Controller
    {
        private readonly IFriendService _friendService;
        private readonly IUsersService _userService;

        public ChatController(IFriendService friendService,IUsersService usersService)
        {
            _friendService = friendService;
            _userService = usersService;
        }
        [Route("Chat")]
        public IActionResult Chat( )
        {
            
            if (HttpContext.Session.GetString("Role") != null)
            {
                ViewBag.Role = HttpContext.Session.GetString("Role");
            }
            if (HttpContext.Session.GetString("Email") != "" && HttpContext.Session.GetString("Email") != null)
            {
                string errorMessage = string.Empty;
                User user = _userService.GetUserByEmail(HttpContext.Session.GetString("Email"), out errorMessage);
                ViewBag.IdUser = user.UserID;
                ViewBag.Name = user.Fullname;
                return View(_friendService.GetFriends(user));
            }
            return RedirectToAction("Login");
        }
        //TODO chat comun-> schimba asta* ds
        //TODO mesajele muta caseta de text si butonul de sent
        //TODO statusul persoanelor
        
    }
}