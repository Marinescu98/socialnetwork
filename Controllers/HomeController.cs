﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SocialNetwork.Models;
using SocialNetwork.Services.IServices;
using SocialNetwork.Services.ServicesImplementation;
using SocialNetwork.Utility;

namespace SocialNetwork.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUsersService _userService;
        private readonly IFeedbackService _feedbackRepository;
        private readonly IPhotoService _photoService;
        private readonly IRoleService _roleService;
        public HomeController(IUsersService usersService,
            IFeedbackService feedbackService,
            IPhotoService photoService,
            IRoleService roleService
            )
        {
            _userService = usersService;
            _feedbackRepository = feedbackService;
            _photoService = photoService;
            _roleService = roleService;
        }

        #region web services
        [Route("")]
        [Route("Home")]
        [Route("Home/Index")]
        public IActionResult Index()
        {
            HttpContext.Session.SetString("Email", "");
            HttpContext.Session.SetString("Role", "");
            return RedirectToAction("Login");
        }
        [Route("Home/Register")]
        public IActionResult Register()
        {
            return View();
        }
        [Route("Home/Login")]
        public IActionResult Login()
        {
            return View();
        }
        [Route("Home/About")]
        public IActionResult About()
        {
            
            if (HttpContext.Session.GetString("Role") != null)
            {
                ViewBag.Role = HttpContext.Session.GetString("Role");
            }
            if (HttpContext.Session.GetString("Email") != "" && HttpContext.Session.GetString("Email") != null)
            {
                string errorMessage;
                User user = _userService.GetUserByEmail(HttpContext.Session.GetString("Email"), out errorMessage);
                ViewBag.Name = user.Fullname;
                return View();
            }
            return RedirectToAction("Login");
        }
        [Route("Home/Feedbacks")]
        public IActionResult Feedbacks()
        {
            if (HttpContext.Session.GetString("Role") != null)
            {
                ViewBag.Role = HttpContext.Session.GetString("Role");
            }
            if (HttpContext.Session.GetString("Email") != "" && HttpContext.Session.GetString("Email") != null)
            {
                string errorMessage;
                User user = _userService.GetUserByEmail(HttpContext.Session.GetString("Email"), out errorMessage);
                ViewBag.Name = user.Fullname;
                return View();
            }
            return RedirectToAction("Login");

        }
        #endregion web services

        #region account and users
        [HttpPost]
        [Route("Home/RegisterUser")]
        [AcceptVerbs("POST")]
        public IActionResult RegisterUser(User user)
        {
            if (user == null)
            {
                return Json(Status.NOT_FOUND_CODE + "," + Status.NO_CONTENT_MESSAGE);
            }

            string errorMessage = String.Empty;

            if (_userService.GetUserByEmail(user.Email, out errorMessage) != null)
            {
                return Json(Status.EMAIL_VALIDATION_CODE + "," + Status.EMAIL_VALIDATION_MESSAGE);
            }
            else
            {
                if (!String.IsNullOrEmpty(errorMessage))
                {
                    return Json(Status.DATABASE_ERROR_CODE + "," + errorMessage);
                }
            }
            user.PhotoProfile = new Photo
            {
                Title = user.Email,
                ImageDataUrl = "http://placehold.it/400x300"
            };

            user.PhotoProfile=new Photo { PhotoID = _photoService.Insert(user.PhotoProfile, out errorMessage) };
            user.UserRole = _roleService.GetRoleById(2,out errorMessage);

            if (_userService.Insert(user, out errorMessage) == 1)
            {
                return Json(Status.SUCCESS_CODE + "," + Status.SUCCESS_MESSAGE);
            }

            if (!String.IsNullOrEmpty(errorMessage))
            {
                return Json(Status.DATABASE_ERROR_CODE + "," + errorMessage);
            }

            return Json(Status.BAD_REQUEST_CODE + "," + Status.BAD_REQUEST_MESSAGE);
        }

        [HttpPost]
        [Route("Home/Login")]
        [AcceptVerbs("POST")]
        public IActionResult Login(User user)
        {
            string errorMessage = String.Empty;
            User userFound = _userService.GetUserByEmail(user.Email, out errorMessage);

            if (userFound == null)
            {
                if (!String.IsNullOrEmpty(errorMessage))
                    return Json(Status.DATABASE_ERROR_CODE + "," + errorMessage);

                return Json(Status.NOT_FOUND_CODE + "," + Status.NOT_FOUND_MESSAGE);
            }

            if (userFound.Password != user.Password)
                return Json(Status.INVALID_PASSWORD_CODE + "," + Status.INVALID_PASSWORD_MESSAGE);

            HttpContext.Session.SetString("Email", user.Email);
            if(userFound.UserRole!=null)
                HttpContext.Session.SetString("Role", userFound.UserRole.Name);

            return Json(Status.SUCCESS_CODE + "," + Status.SUCCESS_MESSAGE);
        }
        #endregion account and users

        #region feedback
        [HttpPost]
        [Route("Home/GiveFeedback")]
        [AcceptVerbs("POST")]
        public IActionResult GiveFeedback(FeedBack feedBack)
        {
            if (feedBack == null)
                return Json(Status.NOT_FOUND_CODE + "," + Status.NO_CONTENT_MESSAGE);

            string errorMessage = String.Empty;

            User currentUser = _userService.GetUserByEmail(HttpContext.Session.GetString("Email"), out errorMessage);

            if (!String.IsNullOrEmpty(errorMessage))
                return Json(Status.DATABASE_ERROR_CODE + "," + errorMessage);

            feedBack.DateReceive = DateTime.Now;
            feedBack.UserSent = currentUser;

            if (_feedbackRepository.InsertFeedback(feedBack, out errorMessage) == 1)
                return Json(Status.SUCCESS_CODE + "," + Status.SUCCESS_MESSAGE);

            if (!String.IsNullOrEmpty(errorMessage))
                return Json(Status.DATABASE_ERROR_CODE + "," + errorMessage);

            return Json(Status.BAD_REQUEST_CODE + "," + Status.BAD_REQUEST_MESSAGE);
        }   
        #endregion


        [Route("Home/GetFeedbacks")]
        public IActionResult GetFeedbacks()
        {
            List<FeedBack> feedBacks = _feedbackRepository.GetFeedbacks();

            if (feedBacks == null || feedBacks.Count == 0)
            {
                return new JsonResult(null);
            }

            return new JsonResult(feedBacks);
        }
    
    }
}
