﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SocialNetwork.Models;
using SocialNetwork.Services.IServices;
using SocialNetwork.Services.ServicesImplementation;
using SocialNetwork.Utility;

namespace SocialNetwork.Controllers
{
    public class ProfileController : Controller
    {

        private readonly IUsersService _usersServiceImplementation;
        private readonly IPhotoService _photoService;
        public ProfileController(IUsersService usersService, IPhotoService photoService)
        {
            _usersServiceImplementation = usersService;
            _photoService = photoService;
        }

        [Route("Profile")]
        [Route("Profile/Profile")]
        public IActionResult Profile()
        {
            if (HttpContext.Session.GetString("Role") != null)
            {
                ViewBag.Role = HttpContext.Session.GetString("Role");
            }

            if (HttpContext.Session.GetString("Email") != "" && HttpContext.Session.GetString("Email") != null)
            {
                string errorMessage = string.Empty;
                User user = _usersServiceImplementation.GetUserByEmail(HttpContext.Session.GetString("Email"), out errorMessage);
                ViewBag.Name = user.Fullname;
                return View(user);
            }
            return RedirectToAction("Login");
        }

        [Route("Profile/GetDataUser")]
        public IActionResult GetDataUser()
        {
            string error = String.Empty;
            return Json(_usersServiceImplementation.GetUserByEmail(HttpContext.Session.GetString("Email"), out error));
        }


        [Route("Profile/ChangeData")]
        public IActionResult ChangeData(User user)
        {
            if (user == null)
            {
                return Json(Status.NO_CONTENT_CODE + "," + Status.NO_CONTENT_MESSAGE);
            }

            string errorMessage = String.Empty;

            User currentUser = _usersServiceImplementation.GetUserByEmail(HttpContext.Session.GetString("Email"), out errorMessage);

            if (currentUser == null)
            {
                if (!String.IsNullOrEmpty(errorMessage))
                {
                    return Json(Status.DATABASE_ERROR_CODE + "," + errorMessage);
                }
            }

            if (user.Password!=null && !user.Password.Equals(currentUser.Password))
                return Json(Status.INVALID_PASSWORD_CODE + "," + Status.INVALID_PASSWORD_MESSAGE);

            if (_usersServiceImplementation.UpdateData(user, currentUser, out errorMessage) == false)
            {
                return Json(Status.DATABASE_ERROR_CODE + "," + errorMessage);
            }

            if (!String.IsNullOrEmpty(user.Email))
                HttpContext.Session.SetString("Email", user.Email);

            return Json(Status.SUCCESS_CODE + "," + Status.SUCCESS_MESSAGE);

        }

        [Route("Profile/DeleteAccount")]
        public IActionResult DeleteAccount(User user)
        {
            if (user == null)
            {
                return Json(Status.NO_CONTENT_CODE + "," + Status.NO_CONTENT_MESSAGE);
            }

            string errorMessage = String.Empty;
            User currentUser = _usersServiceImplementation.GetUserByEmail(HttpContext.Session.GetString("Email"), out errorMessage);

            if (!String.IsNullOrEmpty(user.Email))
                HttpContext.Session.SetString("Email", user.Email);

            if (currentUser == null)
                return Json(Status.EMAIL_INVALID_CODE + "," + Status.EMAIL_INVALID_MESSAGE);

            if (!currentUser.Password.Equals(user.Password))
                return Json(Status.INVALID_PASSWORD_CODE + "," + Status.INVALID_PASSWORD_MESSAGE);

            if (_usersServiceImplementation.DeleteUser(user, out errorMessage) == false)
            {
                return Json(Status.DATABASE_ERROR_CODE + "," + errorMessage);
            }

            if (!String.IsNullOrEmpty(user.Email))
                HttpContext.Session.SetString("Email", "");

            return Json(Status.SUCCESS_CODE + "," + Status.SUCCESS_MESSAGE);

        }

        [Route("Profile/UploadImage")]
        [HttpPost]
        public IActionResult UploadImage()
        {
            Photo photo = new Photo();

            foreach (var item in Request.Form.Files)
            {
                if (item == null)
                    return Json(Status.NOT_FOUND_CODE + "," + Status.NOT_FOUND_MESSAGE);
                photo.Title = item.FileName;
                MemoryStream memoryStream = new MemoryStream();
                item.CopyTo(memoryStream);
                photo.ImageData = memoryStream.ToArray();

                memoryStream.Close();
                memoryStream.Dispose();
                break;
            }

            if (photo == null || photo.ImageData == null)
                return Json(Status.NO_CONTENT_CODE + "," + Status.NO_CONTENT_MESSAGE);

            string imageBase64Data = Convert.ToBase64String(photo.ImageData);
            string imageDataURL = string.Format("data:image/jpg;base64,{0}", imageBase64Data);
            photo.ImageDataUrl = imageDataURL;


            string errorMessage = String.Empty;
            User currentUser = _usersServiceImplementation.GetUserByEmail(HttpContext.Session.GetString("Email"), out errorMessage);

            if (currentUser == null)
            {
                return Json(Status.DATABASE_ERROR_CODE + "," + errorMessage);
            }
            else
            {

                if (_usersServiceImplementation.UpdatePhoto(photo, currentUser, out errorMessage) == false)
                {
                    if (!String.IsNullOrEmpty(errorMessage))
                        return Json(Status.DATABASE_ERROR_CODE + "," + errorMessage);

                    return Json(Status.NOT_RESPONSE_CODE + "," + Status.NOT_RESPONSE_MESSAGE);
                }

            }

            return RedirectToAction("Profile");
        }
    }
}