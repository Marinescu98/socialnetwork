﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SocialNetwork.Models;
using SocialNetwork.Services.IServices;
using SocialNetwork.Services.ServicesImplementation;
using SocialNetwork.Utility;

namespace SocialNetwork.Controllers
{
    public class NoteController : Controller
    {
        private readonly INotesService _noteService;
        private readonly IUsersService _userService;
        public NoteController(IUsersService usersService, INotesService notesService)
        {
            _noteService = notesService;
            _userService = usersService;
        }

        #region web services
        [Route("Note/Note")]
        [Route("Note")]
        public IActionResult Note()
        {
            if (HttpContext.Session.GetString("Role") != null)
            {
                ViewBag.Role = HttpContext.Session.GetString("Role");
            }
            if (HttpContext.Session.GetString("Email") != "" && HttpContext.Session.GetString("Email") != null)
            {
                List<Note> notes = _noteService.GetNotes(HttpContext.Session.GetString("Email"));
                string errorMessage = string.Empty;
                User user = _userService.GetUserByEmail(HttpContext.Session.GetString("Email"), out errorMessage);
                ViewBag.Name = user.Fullname;
                return View(notes);
            }
            return RedirectToAction("Login");
        }
        #endregion

        #region notes add, update edit and delete
        [Route("Note/AddNote")]
        [AcceptVerbs("POST")]
        [HttpPost]
        public IActionResult AddNote(Note note)
        {
            if (note == null)
                return Json(Status.NOT_FOUND_CODE + "," + Status.NO_CONTENT_MESSAGE);

            note.DateAdd = DateTime.Now;

            string errorDatabase = string.Empty;

            note.User = _userService.GetUserByEmail(HttpContext.Session.GetString("Email"), out errorDatabase);

            if (_noteService.Insert(note, out errorDatabase) == 0)
            {
                if (!String.IsNullOrEmpty(errorDatabase))
                    return Json(Status.DATABASE_ERROR_CODE + "," + errorDatabase);

                return Json(Status.NOT_RESPONSE_CODE + "," + Status.NOT_RESPONSE_MESSAGE);
            }

            return Json(Status.SUCCESS_CODE + "," + Status.SUCCESS_MESSAGE);
        }

        [Route("Note/DeleteNote")]
        [AcceptVerbs("GET")]
        [HttpPost]
        public IActionResult DeleteNote(int id)
        {
            if (id == 0)
                return Json(Status.NOT_FOUND_CODE + "," + Status.NO_CONTENT_MESSAGE);

            string errorDatabase = string.Empty;

            if (_noteService.DeleteNotes(id, out errorDatabase) == false)
            {
                if (!String.IsNullOrEmpty(errorDatabase))
                    return Json(Status.DATABASE_ERROR_CODE + "," + errorDatabase);

                return Json(Status.NOT_RESPONSE_CODE + "," + Status.NOT_RESPONSE_MESSAGE);
            }

            return Json(Status.SUCCESS_CODE + "," + Status.SUCCESS_MESSAGE);
        }
        [Route("Note/UpdateNote")]
        [AcceptVerbs("POST")]
        [HttpPost]
        public IActionResult UpdateNote(Note note)
        {
            if (note == null)
                return Json(Status.NOT_FOUND_CODE + "," + Status.NO_CONTENT_MESSAGE);

            string errorDatabase = string.Empty;

            if (_noteService.Update(note, out errorDatabase) == false)
            {
                if (!String.IsNullOrEmpty(errorDatabase))
                    return Json(Status.DATABASE_ERROR_CODE + "," + errorDatabase);

                return Json(Status.NOT_RESPONSE_CODE + "," + Status.NOT_RESPONSE_MESSAGE);
            }

            return Json(Status.SUCCESS_CODE + "," + Status.SUCCESS_MESSAGE);
        }
        [Route("Note/GetNoteById")]
        [AcceptVerbs("GET")]
        [HttpPost]
        public IActionResult GetNoteById(int id)
        {
            string errorDatabase = string.Empty;

            Note note = _noteService.GetNoteById(id, out errorDatabase);

            return Json(note);
        }
        #endregion
    }
}