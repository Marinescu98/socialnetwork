﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SocialNetwork.Models;
using SocialNetwork.Services.IServices;
using SocialNetwork.Services.ServicesImplementation;
using SocialNetwork.Utility;

namespace SocialNetwork.Controllers
{
    public class RoleController : Controller
    {

        private readonly IUsersService _usersService;
        public RoleController(IUsersService usersService)
        {
            _usersService = usersService;
        }

        [Route("Role")]
        public IActionResult Role()
        {
            if (HttpContext.Session.GetString("Role") != null)
            {
                ViewBag.Role = HttpContext.Session.GetString("Role");
            }
            if (HttpContext.Session.GetString("Email") != "" && HttpContext.Session.GetString("Email") != null)
            {
                string errorMessage = string.Empty;
                User user = _usersService.GetUserByEmail(HttpContext.Session.GetString("Email"), out errorMessage);
                ViewBag.Name = user.Fullname;
                return View();
            }
            return RedirectToAction("Login");          
        }


        [Route("Role/ReadRoles")]
        public IActionResult ReadRoles()
        {
            List<User> userRoles = _usersService.GetUsers();

            if (userRoles == null || userRoles.Count == 0)
            {
                return new JsonResult(null);
            }

            return new JsonResult(userRoles);
        }

        //TODO pune validare pe adresa de mail
        [Route("Role/InsertRole")]
        public IActionResult InsertRole(User user)
        {
            if (user == null)
            {
                return Json(Status.NOT_FOUND_CODE + "," + Status.NO_CONTENT_MESSAGE);
            }

            string errorDatabase = string.Empty;

            user.UserRole.RoleID = user.UserRole.Name == "ADMIN" ? 1 : 2;

            User user2 = _usersService.GetUserByEmail(user.Email, out errorDatabase);

            user2.UserRole = user.UserRole;

            if (_usersService.Update(user2, out errorDatabase) == false)
            {
                if (!String.IsNullOrEmpty(errorDatabase))
                    return Json(Status.DATABASE_ERROR_CODE + "," + errorDatabase);

                return Json(Status.NOT_RESPONSE_CODE + "," + Status.NOT_RESPONSE_MESSAGE);
            }

            return Json(Status.SUCCESS_CODE + "," + Status.SUCCESS_MESSAGE);
        }
    }
}