﻿using SocialNetwork.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetwork.Services.IServices
{
    public interface IRoleService
    {
        public Role GetRoleById(int id, out string errorMessage);
    }
}
