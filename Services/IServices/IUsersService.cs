﻿using SocialNetwork.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetwork.Services.IServices
{
    public interface IUsersService
    {
        public int Insert(User user, out string errorMessage);
        public User GetUserByEmail(string email, out string errorMessage);
        public User GetUserById(int id, out string errorMessage);
        public bool UpdateData(User user, User currentUser, out string errorMessage);
        public bool DeleteUser(User user, out string messageError);
        public bool UpdatePhoto(Photo photo, User user, out string errorMessage);
        public bool Update(User user, out string errorMessage);
        public List<User> GetUsers();

    }
}
