﻿using SocialNetwork.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetwork.Services.IServices
{
    public interface INotesService
    {
        public List<Note> GetNotes(string email);
        public int Insert(Note note, out string errorMessage);
        public bool Update(Note note, out string errorMessage);
        public Note GetNoteById(int id, out string errorMessage);
        public bool DeleteNotes(int id, out string error);
    }
}
