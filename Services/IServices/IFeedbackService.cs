﻿using SocialNetwork.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetwork.Services.IServices
{
    public interface IFeedbackService
    {
        public int InsertFeedback(FeedBack feedBack, out string errorMessage);
        public List<FeedBack> GetFeedbacks();
    }
}
