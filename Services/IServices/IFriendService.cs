﻿using SocialNetwork.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetwork.Services.IServices
{
    public interface IFriendService
    {
        public List<Friend> GetFriends(User currentUser);
        public List<User> GetUsersNotFriendsByName(string name);
        public List<User> GetUsersNotFriends();
    }
}
