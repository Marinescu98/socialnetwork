﻿using SocialNetwork.Models;
using SocialNetwork.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetwork.Services.IServices
{
     public interface IPhotoService
    {
        public int Insert(Photo photo, out string errorMessage);
        public Photo GetPhotoByTitleImagData(Photo photo, out string errorMessage);
    }
}
