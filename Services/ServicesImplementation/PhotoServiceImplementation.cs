﻿using SocialNetwork.Models;
using SocialNetwork.Repository.IRepository;
using SocialNetwork.Repository.RepositoryImplementation;
using SocialNetwork.Services.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetwork.Services.ServicesImplementation
{
    public class PhotoServiceImplementation : IPhotoService
    {
        private readonly IPhotoRepository _photoRepository;
        public PhotoServiceImplementation(SocialNetworkDBContext context)
        {
            _photoRepository = new PhotoRepository(context);
        }
        public int Insert(Photo photo, out string errorMessage)
        {
            return _photoRepository.Insert(photo, out errorMessage);
        }
        public Photo GetPhotoByTitleImagData(Photo photo, out string errorMessage)
        {
            return _photoRepository.GetItem(u => u.Title == photo.Title && u.ImageData == photo.ImageData, out errorMessage);
        }
    }
}
