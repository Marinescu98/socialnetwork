﻿using SocialNetwork.Models;
using SocialNetwork.Repository.IRepository;
using SocialNetwork.Repository.RepositoryImplementation;
using SocialNetwork.Services.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetwork.Services.ServicesImplementation
{
    public class UsersServiceImplementation : IUsersService
    {
        private readonly IUserRepository _usersService;
        private readonly SocialNetworkDBContext _context;
        public UsersServiceImplementation(SocialNetworkDBContext context)
        {
            _usersService = new UserRepository(context);
            _context = context;
        }
        public bool Update(User user, out string errorMessage)
        {
            return _usersService.Update(user, out errorMessage);
        }
        public bool DeleteUser(User user, out string messageError)
        {
            return _usersService.Delete(a => a.Email == user.Email, out messageError);
        }

        public User GetUserByEmail(string email, out string errorMessage)
        {
            return _usersService.GetItem(u => u.Email == email, out errorMessage);
        }

        public int Insert(User user, out string errorMessage)
        {
            return _usersService.Insert(user, out errorMessage);
        }


        public bool UpdateData(User user, User currentUser, out string errorMessage)
        {
            errorMessage = String.Empty;

            if (!String.IsNullOrEmpty(errorMessage))
                return false;


            if (user.Password != null && !currentUser.Password.Equals(user.Password))
                return false;

            if (!String.IsNullOrEmpty(user.Fullname) && !user.Fullname.Equals(currentUser.Fullname))
            {
                currentUser.Fullname = user.Fullname;
            }

            if (user.BirthDay != null && !user.BirthDay.Equals(currentUser.BirthDay))
            {
                currentUser.BirthDay = user.BirthDay;
            }

            if (!String.IsNullOrEmpty(user.About) && !user.About.Equals(currentUser.About))
            {
                currentUser.About = user.About;
            }

            if (!String.IsNullOrEmpty(user.CompanyWorking) && !user.CompanyWorking.Equals(currentUser.CompanyWorking))
            {
                currentUser.CompanyWorking = user.CompanyWorking;
            }

            if (!String.IsNullOrEmpty(user.Email) && !user.Email.Equals(currentUser.Email))
            {
                currentUser.Email = user.Email;
            }

            if (!String.IsNullOrEmpty(user.Genre) && !user.Genre.Equals(currentUser.Genre))
            {
                currentUser.Genre = user.Genre;
            }

            if (!String.IsNullOrEmpty(user.Password) && !user.Password.Equals(currentUser.Password))
            {
                currentUser.Password = user.Password;
            }

            if (!String.IsNullOrEmpty(user.WebSite) && !user.WebSite.Equals(currentUser.WebSite))
            {
                currentUser.WebSite = user.WebSite;
            }

            if (!String.IsNullOrEmpty(user.Country) && !user.Country.Equals(currentUser.Country))
            {
                currentUser.Country = user.Country;
            }

            return _usersService.Update(currentUser, out errorMessage);
        }

        public bool UpdatePhoto(Photo photo, User user, out string errorMessage)
        {
            return _usersService.UpdatePhoto(photo, user, out errorMessage);
        }
        public List<User> GetUsers()
        {
            return _usersService.GetUsers();
        }

        public User GetUserById(int id, out string errorMessage)
        {
            return _usersService.GetItem(u => u.UserID == id, out errorMessage);
        }


    }
}
