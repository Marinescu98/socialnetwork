﻿using SocialNetwork.Models;
using SocialNetwork.Repository.IRepository;
using SocialNetwork.Repository.RepositoryImplementation;
using SocialNetwork.Services.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetwork.Services.ServicesImplementation
{
    public class FriendServiceImplementation: IFriendService
    {
        private readonly IFriendRepository _friendRepository;

        public FriendServiceImplementation(SocialNetworkDBContext context)
        {
            _friendRepository = new FriendRepository(context);
        }

        public List<Friend> GetFriends(User currentUser)
        {
            return _friendRepository.GetFriends(currentUser);
        }

        public List<User> GetUsersNotFriends()
        {
            return _friendRepository.GetUsersNotFriends();
        }

        public List<User> GetUsersNotFriendsByName(string name)
        {
            return _friendRepository.GetUsersNotFriendsByName(name);
        }
    }
}
