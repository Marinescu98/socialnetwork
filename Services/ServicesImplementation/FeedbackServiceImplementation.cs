﻿using SocialNetwork.Models;
using SocialNetwork.Repository.IRepository;
using SocialNetwork.Repository.RepositoryImplementation;
using SocialNetwork.Services.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetwork.Services.ServicesImplementation
{

    public class FeedbackServiceImplementation : IFeedbackService
    {
       private readonly IFeedbackRepository _feedbackRepository;

        public FeedbackServiceImplementation(SocialNetworkDBContext context)
        {
            _feedbackRepository = new FeedbackRepository(context);
        }

        public List<FeedBack> GetFeedbacks()
        {
           return _feedbackRepository.GetFeedbacks();
        }

        public int InsertFeedback(FeedBack feedBack, out string errorMessage)
        {
            return _feedbackRepository.Insert(feedBack,out errorMessage);
        }
    }
}
