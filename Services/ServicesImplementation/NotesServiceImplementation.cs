﻿using SocialNetwork.Models;
using SocialNetwork.Repository.IRepository;
using SocialNetwork.Repository.RepositoryImplementation;
using SocialNetwork.Services.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetwork.Services.ServicesImplementation
{
    public class NotesServiceImplementation : INotesService
    {
        private readonly INoteRepository _noteRepository;
        public NotesServiceImplementation(SocialNetworkDBContext context)
        {
            _noteRepository = new NoteRepository(context);
        }
        public bool DeleteNotes(int id, out  string error)
        {
            return _noteRepository.Delete(u=> u.NoteID == id, out error);
        }

        public Note GetNoteById(int id,out string errorMessage)
        {
            return _noteRepository.GetItem(u => u.NoteID == id,out errorMessage);
        }

        public List<Note> GetNotes(string email)
        {
            return _noteRepository.GetNotes(email);
        }

        public int Insert(Note note, out string errorMessage)
        {
            return _noteRepository.Insert(note, out errorMessage);
        }

        public bool Update(Note note, out string errorMessage)
        {
            Note noteUpdate = GetNoteById(note.NoteID, out errorMessage);
            noteUpdate.Content = note.Content;
            noteUpdate.Title = note.Title;
            return _noteRepository.Update(note, out errorMessage);
        }
    }
}
