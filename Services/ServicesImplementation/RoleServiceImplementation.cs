﻿using SocialNetwork.Models;
using SocialNetwork.Repository.IRepository;
using SocialNetwork.Repository.RepositoryImplementation;
using SocialNetwork.Services.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetwork.Services.ServicesImplementation
{
    public class RoleServiceImplementation : IRoleService
    {
        private readonly IRoleRepository roleRepository;
        public RoleServiceImplementation(SocialNetworkDBContext context)
        {
            roleRepository = new RoleRepostory(context);
        }
        public Role GetRoleById(int id, out string errorMessage)
        {
            return roleRepository.GetRoleById(id, out errorMessage);
        }
    }
}
