﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SignalR;
using SocialNetwork.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetwork.Hubs
{
    public class ChatHub:Hub
    {
        public async Task SendMessage(string user,string message)
        {


            await Clients.All.SendAsync("ReceiveMessage",user, message);
        }
    }
}
