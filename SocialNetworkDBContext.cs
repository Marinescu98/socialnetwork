﻿using Microsoft.EntityFrameworkCore;
using SocialNetwork.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetwork
{
    public class SocialNetworkDBContext:DbContext
    {
        public SocialNetworkDBContext(DbContextOptions<SocialNetworkDBContext> options)
    : base(options)
        {

        }

        public DbSet<FeedBack> FeedBacks { get; set; }
        public DbSet<Friend> Friends { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<Note> Notes { get; set; }
        public DbSet<Photo> Photos { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Role>().HasData(
                    new Role
                    {
                        RoleID = 2,
                        Name = "USER"
                    },
                    new Role
                    {
                        RoleID=1,
                        Name="ADMIN"
                    }
                );
        }
    }
}
